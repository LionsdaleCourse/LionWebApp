<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Http\Requests\StoreCourseRequest;
use App\Http\Requests\UpdateCourseRequest;
use Illuminate\Auth\Middleware\Authorize;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('viewIndex', Course::class);
        $courses = Course::all();
        return view("courses.index", ['courses' => $courses]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("courses.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCourseRequest $request)
    {
        $course = new Course(
            [
                "name" => $request->name,
                "description" => $request->description
            ]
        );

        $course->save();

        return back()->with("success", "Course created successfully.");
    }

    /**
     * Display the specified resource.
     */
    public function show(Course $course)
    {
        $this->authorize('view', $course);
        return view('courses.show', ['course' => $course]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Course $course)
    {
        return view("courses.edit", ['course' => $course]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCourseRequest $request, Course $course)
    {
        $course->update($request->all());

        return redirect()->route('courses.index')->with('success', 'The course was updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return back()->with('success', 'Successfully deleted the course: ' . $course->name . '.');
    }


    /* Saját funkciók */
    public function show_deleted()
    {
        $deleted_courses = Course::onlyTrashed()->get();
        return view('courses.show_deleted', ['courses' => $deleted_courses]);
    }

    public function restore(Course $course)
    {
        $course->restore();
        return back()->with('success', '' . $course->name . ' was successfully restored.');
    }

}
