<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationConfirm;


class EmailController extends Controller
{
    public function sendWelcomeEmail()
    {
        $title = 'Registration Confirm';
        $body = 'Thank you for participating!';

        Mail::to('richardwyeth@gmail.com')->send(new RegistrationConfirm($title, $body));

        return "Email sent successfully!";
    }
}
