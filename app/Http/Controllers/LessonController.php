<?php

namespace App\Http\Controllers;

use App\Models\Lesson;
use App\Models\Course;
use App\Http\Requests\StoreLessonRequest;
use App\Http\Requests\UpdateLessonRequest;
use App\Models\Review;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

    }

    public function show_lessons(Course $course) {

        // A kurzushoz tartozó leckék lekérése
        /* $course = Course::with('lessons')->find($course_id); */
        $lessons = $course->lessons;

        if (count($lessons) == 0) {
            return view('lessons.no_lessons');
        }

        // A kurzushoz tartozó reviewk lekérése
        /* $reviews = Review::all()->where('course_id', $course->id); */
        $reviews = $course->reviews()->orderByDesc('id')->get();
        return view ('lessons.show_lessons', ['lessons' => $lessons, 'reviews' => $reviews]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLessonRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Lesson $lesson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Lesson $lesson)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLessonRequest $request, Lesson $lesson)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Lesson $lesson)
    {
        //
    }

    public function show_deleted() {

    }
}
