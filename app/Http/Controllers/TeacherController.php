<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use App\Http\Requests\StoreteacherRequest;
use App\Http\Requests\UpdateteacherRequest;
use Intervention\Image\Facades\Image as ResizeImage;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $teachers = Teacher::all();
        return view('teachers.index', ["teachers" => $teachers]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreteacherRequest $request)
    {
        $teacher = new Teacher($request->all());
        $teacher->save();

        return redirect('teachers')->with('success', $teacher->name. ' added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Teacher $teacher)
    {
        return view('teachers.show', ['teacher' => $teacher]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Teacher $teacher)
    {
        return view('teachers.edit', ['teacher' => $teacher]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateteacherRequest $request, Teacher $teacher)
    {

         /* Resize image mentés */
        /* composer require intervention/image */
        /* use Intervention\Image\Facades\Image as ResizeImage; */
    
        if ($request->picture != null) {
            $filename = 'PI_Teacher_' . $teacher->id . '.' . $request->picture->extension();

            $img = ResizeImage::make($request->picture)
                ->resize(500, 500)->save('storage/ProfileImages/' . $filename);

            $teacher->picture = $filename;
        }
     


        $teacher->name = $request->name;
        $teacher->email = $request->email;
        $teacher->degree = $request->degree;
        $teacher->subjects = $request->subjects;
        $teacher->update();
        
        return redirect('teachers')->with('success', $teacher->name. '\'s data was successfully updated.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Teacher $teacher)
    {
        $teacher->delete();
        return redirect('teachers')->with('success', $teacher->name. ' was successfully deleted.');

    }
}
