<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as ResizeImage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        return view('auth/updateProfile', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        /* Resize image mentés */
        /* composer require intervention/image */
        /* use Intervention\Image\Facades\Image as ResizeImage; */
        if ($request->file != null) {
            $filename = 'PI_user_' . $user->id . '.' . $request->file->extension();

            $img = ResizeImage::make($request->file)
                ->resize(500, 500)->save('storage/ProfileImages/' . $filename);

            $user->profile_image = $filename;
        }

        /* Resize változat vége */

        /* ORIGINAL RESIZE NÉLKÜLI VÁLTOZAT BÁRMILYEN FILE ESETÉBEN */
        /* $path = $request->file->storeAs(
            'ProfileImages', //Elérési útvonal
            'profil_'.$user->id, //Fájl neve
            'public'); //Disk (lásd config, filesystems.php) 

        $filename = 'profil_'.$user->id;
        $user->profile_image = $filename;
        $user->update(); */
        /* Original vége */



        $user->name = $request->username;
        $user->update();
        return back()->with('success', 'User updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
