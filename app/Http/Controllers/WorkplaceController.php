<?php

namespace App\Http\Controllers;

use App\Models\Workplace;
use App\Http\Requests\StoreWorkplaceRequest;
use App\Http\Requests\UpdateWorkplaceRequest;

class WorkplaceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $workplaces = Workplace::all();
        return view("workplaces.index", ["resources" => $workplaces]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("workplaces.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreWorkplaceRequest $request)
    {
        $workplace = Workplace::create($request->all());
        return redirect()->route("workplaces.index")->with("success", "Workplace created successfully.");

    }

    /**
     * Display the specified resource.
     */
    public function show(Workplace $workplace)
    {
        return view ('workplaces.show', ['workplace' => $workplace]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Workplace $workplace)
    {
        return view("workplaces.edit", ["resource" => $workplace]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateWorkplaceRequest $request, Workplace $workplace)
    {
        $workplace->update($request->all());
        return redirect()->back()->with("success","Update complete.");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Workplace $workplace)
    {
        $workplace->delete();
        return back()->with("success","Delete Complete");
    }

    public function show_deleted() {
        $deleted_workplaces = Workplace::onlyTrashed()->get();
        return view('workplaces.show_deleted', ['workplaces' => $deleted_workplaces]);
    }

    public function restore(Workplace $workplace) {
        $workplace->restore();
        return redirect()->route('workplaces.index')->with('success', "Workplace restored successfully.");
    }
}
