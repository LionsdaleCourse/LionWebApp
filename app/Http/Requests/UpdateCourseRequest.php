<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
                        //A végén az ID azért kell, hogy megmondjam az adott ID-jű workplace amit frissítek lehet ugyanazon a néven.
            'name' => 'required|max:10|unique:courses,name,'.$this->course->id,
            'description'=> 'required|max:255',     
        ];
    }
}
