<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:teachers,name,'.$this->teacher->id,
            'email' => 'required|unique:teachers,email,'.$this->teacher->id,
            'subjects' => 'required',
            'degree' => 'required',
            'picture' => 'image|max:20000'
        ];
    }
}
