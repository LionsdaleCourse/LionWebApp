<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'thumbnail'
    ];


    /* Get the users of the course */
    public function users(): BelongsToMany {
        return $this->belongsToMany(User::class);
    }

    public function lessons(): HasMany {
        return $this->hasMany(Lesson::class);
    }

    public function reviews(): HasMany {
        return $this->hasMany(Review::class);
    }
}
