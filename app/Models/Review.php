<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'content',
        'rating',
        'course_id'
    ];

    public function user(): BelongsTo {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function course(): BelongsTo {
        return $this->belongsTo(Course::class);
    }
}

