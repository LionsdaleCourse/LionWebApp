<?php

namespace App\Observers;

use App\Models\Course;

class CourseObserver
{
    /**
     * Handle the Course "created" event.
     */
    public function creating(Course $course): void
    {
        $user = auth()->user();
        $course->created_by = $user ? $user->id : null;
    }

    /**
     * Handle the Course "updated" event.
     */
    public function updating(Course $course): void
    {
        $user = auth()->user();
        $course->updated_by = $user ? $user->id : null;
    }

    /**
     * Handle the Course "deleted" event.
     */
    public function deleted(Course $course): void
    {
        $user = auth()->user();
        $course->deleted_by = $user ? $user->id : null;
        $course->save();
    }

    /**
     * Handle the Course "restored" event.
     */
    public function restored(Course $course): void
    {
        //
    }

    /**
     * Handle the Course "force deleted" event.
     */
    public function forceDeleted(Course $course): void
    {
        //
    }
}
