<?php

namespace App\Observers;

use App\Models\Workplace;

class WorkplaceObserver
{
    /**
     * Handle the Workplace "created" event.
     */
    public function creating(Workplace $workplace): void
    {
        /* $user = auth()->user();
        $workplace->created_by = $user ? $user->id : null; */
    }

    /**
     * Handle the Workplace "updated" event.
     */
    public function updating(Workplace $workplace): void
    {
        /* $user = auth()->user();
        $workplace->updated_by = $user ? $user->id : null; */
    }

    /**
     * Handle the Workplace "deleted" event.
     */
    public function deleted(Workplace $workplace): void
    {
       /*  $user = auth()->user();
        $workplace->deleted_by = $user ? $user->id : null;
        $workplace->save(); */
    }

    /**
     * Handle the Workplace "restored" event.
     */
    public function restored(Workplace $workplace): void
    {
        //
    }

    /**
     * Handle the Workplace "force deleted" event.
     */
    public function forceDeleted(Workplace $workplace): void
    {
        //
    }
}
