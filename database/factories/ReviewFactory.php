<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Review>
 */
class ReviewFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'content' => fake()->sentences(3, true),
            'rating' => fake()->numberBetween(1,5),
            'course_id' => fake()->numberBetween(1,5),
            'created_by' => fake()->numberBetween(1,10)
        ];
    }
}
