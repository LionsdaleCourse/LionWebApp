<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Workplace>
 */
class WorkplaceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        static $id = 1;
        return [
            "name" => 'Test Company - ' . $id++,            
            "location" => fake()->address(),
            "registrynumber" => fake()->creditCardNumber(),
            "taxnumber" => fake()->creditCardNumber()
        ];
    }
}
