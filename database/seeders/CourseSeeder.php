<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Course::factory()->create([
            'name' => 'PhP',
            'description' => 'PhP Basics',
            'thumbnail' => 'TN_Php.png',
        ]);

        \App\Models\Course::factory()->create([
            'name' => 'Laravel',
            'description' => 'Laravel Basics',
            'thumbnail' => 'TN_Laravel.jpg',
        ]);

        \App\Models\Course::factory()->create([
            'name' => 'CSS',
            'description' => 'CSS Basics',
            'thumbnail' => 'TN_Css.jpg',

        ]);

        \App\Models\Course::factory()->create([
            'name' => 'HTML',
            'description' => 'HTML Basics',
            'thumbnail' => 'TN_Html.png',

        ]);

        \App\Models\Course::factory()->create([
            'name' => 'Javascript',
            'description' => 'Javascript Basics',
            'thumbnail' => 'TN_Javascript.png',

        ]);
    }
}
