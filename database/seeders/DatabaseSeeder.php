<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        


        /* Workplace feltöltése */
        $this->call([
            WorkplaceSeeder::class,
            UserSeeder::class,
            CourseSeeder::class,
            LevelSeeder::class,
            LessonSeeder::class,
            TeacherSeeder::class,
            ReviewSeeder::class
        ]);

        /* Minden Userhez bizonyos számú Kurzus csatolása
         ManyToMany kapcsolat esetén */
         
        \App\Models\User::all()->each(function ($user) {
            $courses = \App\Models\Course::inRandomOrder()->limit(3)->pluck('id');
            $user->courses()->attach($courses);
        });



        

    }
}
