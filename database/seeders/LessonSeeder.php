<?php

namespace Database\Seeders;

use App\Models\Lesson;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Lesson::create([
            'name' => 'What will you learn',
            'description' => 'Brief summery of the course material',
            'course_id' => 1,
            'level_id' => 1
        ]);

        Lesson::create([
            'name' => 'How to install on Windows / Mac',
            'description' => 'Installation guide',
            'course_id' => 1,
            'level_id' => 2
        ]);

        Lesson::create([
            'name' => 'The basics',
            'description' => 'Why this language and how?',
            'course_id' => 1,
            'level_id' => 3
        ]);

        Lesson::create([
            'name' => 'File handling',
            'description' => 'Storage and the others',
            'course_id' => 1,
            'level_id' => 4
        ]);

        Lesson::create([
            'name' => 'Deploy to server',
            'description' => 'This is the real deal.',
            'course_id' => 1,
            'level_id' => 5
        ]);

        Lesson::create([
            'name' => 'What will you learn',
            'description' => 'Brief summery of the course material',
            'course_id' => 2,
            'level_id' => 1
        ]);

        Lesson::create([
            'name' => 'How to install on Windows / Mac',
            'description' => 'Installation guide',
            'course_id' => 2,
            'level_id' => 1
        ]);

        Lesson::create([
            'name' => 'The basics',
            'description' => 'Why this language and how?',
            'course_id' => 2,
            'level_id' => 1
        ]);
    }
}
