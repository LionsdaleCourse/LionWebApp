<?php

namespace Database\Seeders;

use App\Models\Level;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Level::create([
            'name' => 'Meerkat',
            'slug' => '1'
        ]);

        Level::create([
            'name' => 'Zebra',
            'slug' => '2'
        ]);

        Level::create([
            'name' => 'Giraffe',
            'slug' => '3'
        ]);

        Level::create([
            'name' => 'Elephant',
            'slug' => '4'
        ]);

        Level::create([
            'name' => 'Lion',
            'slug' => '5'
        ]);
    }
}
