<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Teacher::factory()->create([
            'name' => 'Péter-Szabó Richárd',
            'email' => 'richardwyeth@gmail.com',
            'subjects' => 'Webfejlesztés, hálózatüzemeltetés',
            'degree' => 'PhD.',
            'picture' => 'PI_Placeholder.jpg'
        ]);

        \App\Models\Teacher::factory()->create([
            'name' => 'Varga Viktor',
            'email' => 'viktor.varga@gmail.com',
            'subjects' => 'Szoftverfejlesztés',
            'degree' => 'Felsőfokú',
            'picture' => 'PI_Placeholder.jpg'
        ]);
    }
}
