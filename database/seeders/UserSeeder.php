<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /* A userek feltöltése */
        \App\Models\User::factory()->create([
            'name' => 'Rick',
            'email' => 'test@example.com',
            'workplace_id' => '10',
            'role_id' => 1
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Snake',
            'email' => 'snake@example.com',
            'workplace_id' => '10',
            'role_id' => 2
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Rafi',
            'email' => 'rafi@example.com',
            'workplace_id' => '10',
            'role_id' => 3
        ]);

        \App\Models\User::factory(10)->create();

    }
}
