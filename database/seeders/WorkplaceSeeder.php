<?php

namespace Database\Seeders;

use Database\Factories\WorkplaceFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Workplace;

class WorkplaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Workplace::factory()->count(9)->create();

        Workplace::factory()->create([
            'name' => 'Lionsdale',
            'location' => '1074 Budapest, Rákóczi út 82.',
        ]);
    }
}
