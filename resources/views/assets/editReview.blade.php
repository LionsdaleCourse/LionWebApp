<div class="card bg-dark text-white">
    <div class="card-body">
        <h4 class="card-title">Szerkesztés</h4>
        <div>
            @include('assets.messages')
            <form action="" method="POST">
                <input type="hidden" name="course_id" id="course_id" value="{{ $lesson->course_id }}">
                <div>
                    <label for="rating" style="color: #fcd424">Rating</label>
                    <input type="hidden" name="rating" id="rating" value="0">
                </div>
                <div class="rating mb-3" id="ratingStars">
                    <span class="fa-regular fa-star ratingStars" onmouseover="chooseRating(this);"
                        onmouseout="removeRating()" onmousedown="pickRating(this)" id=1></span>
                    <span class="fa-regular fa-star ratingStars" onmouseover="chooseRating(this);"
                        onmouseout="removeRating()" onmousedown="pickRating(this)" id=2></span>
                    <span class="fa-regular fa-star ratingStars" onmouseover="chooseRating(this);"
                        onmouseout="removeRating()" onmousedown="pickRating(this)" id=3></span>
                    <span class="fa-regular fa-star ratingStars" onmouseover="chooseRating(this);"
                        onmouseout="removeRating()" onmousedown="pickRating(this)" id=4></span>
                    <span class="fa-regular fa-star ratingStars" onmouseover="chooseRating(this);"
                        onmouseout="removeRating()" onmousedown="pickRating(this)" id=5></span>
                </div>
                <div>
                    @csrf
                    <input id="content" type="hidden" name="content">
                    <trix-editor class="trix-content" input="content"></trix-editor>
                </div>
                <div class="text-center mt-3">
                    <button class="btn form-button">Send review</button>
                </div>
            </form>
        </div>
    </div>
</div>