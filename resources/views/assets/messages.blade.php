@if ($errors->any())
    <div class="mb-3 mt-3">
        <div class="alert alert-danger alert-dismissible fade show " role="alert">
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            <strong>Holy guacamole!</strong>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show success-alert" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

        <strong class="text-success">Holy guacamole!</strong>
        <p class="text-success" style="font-weight: bold">{{ Session::get('success') }}</p>
    </div>
@endif
