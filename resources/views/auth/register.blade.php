@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="content">
            <div id="white-box" class="text-white" style="background-color: #212529">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <h1>May the frameworks be with you!</h1>
                    <p style="color: #2E67F8">*After asking for student rank, please wait for the SQL Masters to grant it.
                    </p>
                    <div id="form-container">
                        <div class="form-group">
                            <input type="text" name="name" id="name" placeholder="Username" required autocomplete="name" autofocus class="form-control @error('name') is-invalid @enderror">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" name="password" id="password" placeholder="Password" required autocomplete="password" autofocus class="form-control @error('password') is-invalid @enderror">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" name="password_confirmation" id="passwordConfirm"
                                placeholder="Password Confirm" required autocomplete="password_confirmation" autofocus class="form-control @error('passwordConfirm') is-invalid @enderror">
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" placeholder="E-mail address" required autocomplete="email" autofocus class="form-control @error('email') is-invalid @enderror">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="">
                            <button class="text-dark" id="register-button" type="submit">Register</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


