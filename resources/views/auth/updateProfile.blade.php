@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            @if ($errors->any())
                        <div class="mb-3 mt-3">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                                <strong>Holy guacamole!</strong>

                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
            <div class="card bg-dark text-white border-dark align-items-center p-5 ">
                <img class="profile-img" src="{{ asset('storage/ProfileImages/' . $user->profile_image) }}"
                    alt="User's Profile Image" onclick="fileBrowser();">
                <div class="card-body ">
                    <h1 class="card-title text-center mt-4">{{ $user->name }}</h1>
                    <p class="card-text text-warning">*To update your picture click on it!</p>
                    <div class="">
                        <form action="{{ route('user.update', $user) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="file" class="form-control mb-3" name="file" id="file">
                            <input class="form-control mb-3 text-center" style="font-family: 'Courier New', Courier, monospace" type="text" placeholder="Username" name="username"
                                value="{{ old('username', $user->name) }}">
                            <button type="submit" class="btn form-button mb-3">Frissítés</button>
                        </form>

                        <a href="#" class="btn inverse-form-button mb-3">Jelszóváltoztatás</a>
                        <a href="#" class="btn inverse-form-button mb-3">E-mail változtatás</a>
                     </div>
                </div>
            </div>
        </div>
        <div class="col-4"></div>
    </div>

    <script>
        function fileBrowser() {
            document.getElementById("file").click();
        }
    </script>
@endsection


