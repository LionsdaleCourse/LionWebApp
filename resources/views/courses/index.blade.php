@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-2"></div>
        <div class="col-8">
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                    <strong>Holy guacamole!</strong>
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
            <div class="table-responsive-xl">
                <table class="table table-dark" id="courseTable">
                    <thead>
                        <tr>
                            <th scope="col">Course name</th>
                            <th scope="col">Course Description</th>
                            <th scope="col">Actions</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($courses as $item)
                            <tr class="">
                                <td scope="row">{{ $item->name }}</td>
                                <td>{{ $item->description }}</td>
                                <td>
                                    <form action="{{ route('courses.edit', $item) }}" method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-warning">Edit</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ route('courses.destroy', $item) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                                <td>

                                    <form action="{{ route('courses.show', $item) }}" method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-info">Show data</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-2"></div>
    </div>

{{--     <div class="row mt-5">
        <div class="col-2"></div>
        <div class="col-8">
            <div id="calendar" style="background-color: #212521"></div>
        </div>
        <div class="col-2"></div>
    </div> --}}

{{--     <script>
        $(document).ready(function() {
            $('#courseTable').DataTable({
                "language": {
                    "search": "Keresés: ",
                    "lengthMenu": "_MENU_ sor mutatása",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                ordering: false,
            });
        });

        /*         let table = new DataTable('#courseTable');
         */
    </script>
    {{-- 
    <script>
        let courseData = <?php echo json_encode($courses); ?>;
        console.log(courseData);

        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                /* Tulajdonságok eleje */
                themeSystem: 'bootstrap5',

                initialView: 'dayGridMonth',
                selectable: true,
                editable: true,
                dayMaxEvents: true,
                events: [
                    <?php
                    for ($i = 0; $i < count($courses); $i++) {
                        echo "{title: '" . $courses[$i]->name . "', start: '2024-01-2" . $i . "',end: '2018-01-24',allDay: false},";
                    } ?>
                ],
                dateClick: function(info) {
                    alert('Date: ' + info.dateStr);
                    alert('Resource ID: ' + info.resource.id);
                },
                eventClick: function(info) {
                    alert('Event is clicked');
                }

                /* Tulajdonságok vége */
            });
            calendar.render();
        });
    </script> --}}

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                timeZone: 'UTC',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                editable: true,
                dayMaxEvents: true, // when too many events in a day, show the popover
                events: [{ // this object will be "parsed" into an Event Object
                    title: 'The Title', // a property!
                    start: '2024-01-01', // a property!
                    end: '2024-01-02T23:59:59' // a property! ** see important note below about 'end' **
                }]
            });

            calendar.render();
        });
    </script> --}}
@endsection
