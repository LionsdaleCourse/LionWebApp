@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col">
        <div class="card border-secondary">
            <div class="card-body">
                <h4 class="card-title">{{$course->name}}</h4>

                <img src="{{$course->thumbnail}}" alt="kurzuskepe">
                <p>A kurzus neve: {{$course->name}}</p>
                <p>A kurzus leírása: {{$course->description}}</p>
                
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>

@endsection