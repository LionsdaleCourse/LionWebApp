<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>

    <style>

    </style>
</head>

<body>
    <h1>
        Köszönjük, hogy minket választottál.
    </h1>

    <p>
        Most már be tudsz lépni a választott jelszavaddal és felhasználóneveddel.
        <br>
        Felhasználónév: {{ $body }}
        <br> 
        Jelszó: - Nem küldjük el, mert nem vagyunk amatőrök. Ha nem emlékszel nincs itt helyed.
    </p>
</body>

</html>
