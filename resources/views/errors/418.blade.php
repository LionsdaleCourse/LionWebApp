<html lang="en" class="">

<head>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script nonce="">
        (function(H) {
            H.className = H.className.replace(/\bgoogle\b/, 'google-js')
        })(document.documentElement)
    </script>
    <meta charset="utf-8">
    <meta content="initial-scale=1, minimum-scale=1, width=device-width" name="viewport">
    <title>Error 418 (I’m a teapot)!?</title>
    <link href="//www.gstatic.com/teapot/teapot.min.css" rel="stylesheet" nonce="">
</head>

<body><a href="https://www.google.com/"><span aria-label="Google" id="logo"></span></a>
    <p><b>418.</b> <ins>I’m a teapot.</ins></p>
    <p>The requested entity body is short and stout. <ins>Tip me over and pour me out.</ins></p>
    <div id="teaset">
        <div id="teabot" class="" style="transform: rotate(0deg);"></div>
        <div id="teacup"></div>
    </div>
    <script src="//www.gstatic.com/teapot/teapot.min.js" nonce=""></script>
</body>

</html>
