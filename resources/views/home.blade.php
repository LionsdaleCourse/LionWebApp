@extends('layouts.app')



@section('content')
    <script>
        const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
        const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
    </script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card bg-dark text-light courseContainer ">
                    <div class="card-header">{{ __('Enrolled Courses') }}</div>
                    <div class="card-body">
                        <div class="row align-items-start">
                            @foreach (Auth::user()->courses as $course)
                                {{-- Oszlop eleje --}}
                                <div class="col-4 mb-3">
                                    <div class="card-holder">
                                        {{-- Card eleje --}}
                                        <div class="card course-card" style="max-width: 540px;" onclick="submitForm(this)">
                                            <form action="{{ route('lessons.show_lessons', $course) }}">
                                                <div class="row g-0">
                                                    <div class="col-md-4 align-items-center d-flex">
                                                        <img src="{{ asset('storage/Thumbnails/' . $course->thumbnail) }}"
                                                            class="img-fluid rounded-start ms-2" alt="Card title">
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="card-body">
                                                            <h5 class="card-title">{{ $course->name }}</h5>
                                                            <p class="card-text course-description">
                                                                {{ $course->description }}
                                                                Lorem ipsum dolor sit
                                                                amet, consectetur adipisicing elit. Quis doloribus
                                                                illo hic.
                                                                Repudiandae, dolor recusandae voluptatem accusantium
                                                                culpa molestias
                                                                similique accusamus quaerat voluptatibus nihil
                                                                maiores! Expedita
                                                                vitae dolorem optio veritatis.
                                                            </p>
                                            </form>
                                            <div><small class="text-muted">Last updated
                                                    {{ $course->updated_at }}</small></div>

                                        </div>
                                    </div>
                                </div>
                        </div>
                        {{-- Card vége --}}
                    </div>

                </div>
                {{-- Oszlop vége --}}
                @endforeach
            </div>
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>


    <script>
        function submitForm(card) {
            let form = card.firstElementChild;
            form.submit();
        }
    </script>
@endsection
