@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-6 d-flex align-items-center">
                <div class="row">
                    <h1 style="font-family: 'Courier New', Courier, monospace; font-size: 8rem;">Welcome</h1>
                    <p style="font-family: 'Courier New', Courier, monospace; font-size: 1rem; font-weight:bold;">At
                        Lionsdale Studio, we're
                        passionate about delivering personalised software solutions, developing impactful websites, creating
                        educational serious games and nurturing the next generation of programming talent. Whether you're
                        here to explore our software or web-services, or to learn about our unique high-school student
                        internship programs, you're in the right place.</p>
                </div>
            </div>
            <div class="col-6 text-center">
                <img src="{{ asset('storage/welcome/computer.png') }}" alt="computer" class="img img-fluid">
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-2 p-4 text-center"><img src="{{ asset('storage/Thumbnails/TN_Html.png') }}" class="img-fluid"
                    alt="Logo"></div>
            <div class="col-2 p-4 text-center"><img src="{{ asset('storage/Thumbnails/TN_Css_NoBg.png') }}"
                    class="img-fluid" style="max-height: 6rem" alt=""></div>
            <div class="col-2 p-4 text-center"><img src="{{ asset('storage/Thumbnails/TN_C.png') }}" class="img-fluid"
                    style="max-height: 6rem" alt=""></div>
            <div class="col-2 p-4 text-center"><img src="{{ asset('storage/Thumbnails/TN_Php.png') }}" class="img-fluid"
                    alt=""></div>
            <div class="col-2 p-4 text-center"><img src="{{ asset('storage/Thumbnails/TN_Laravel_NoBg.png') }}"
                    class="img-fluid" alt=""></div>
            <div class="col-2 p-4 text-center"><img src="{{ asset('storage/Thumbnails/TN_Unreal.png') }}" class="img-fluid"
                    alt=""></div>
        </div>

        <div class="row d-flex align-items-center mt-5 pt-4 border-right">
            <div class="col-6">
                <div class="card text-white info-card welcome-card">
                    <div class="card-body">
                        <h4 class="card-title">Training facility rental</h4>
                        <p class="card-text">Discover the ideal space for conducting exams at Lionsdale Studio. Our
                            dedicated classrooms are meticulously designed to provide a conducive environment for teaching,
                            academic
                            assessments, complete with the latest computer equipment to meet your specific exam needs.
                        </p>
                        <p class="card-text">Our exam rooms are equipped with quality notebooks, ensuring a
                            seamless and reliable platform for digital assessments.</p>

                        <p class="card-text">With a maximum capacity of 65 students, our spacious exam rooms allow you to
                            conduct large-scale assessments efficiently and comfortably.</p>
                    </div>
                    <div class="text-center m-2">
                        <a href="#" class="btn btn-warning">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-6 align-items-center text-center p-2">
                <img src="{{ asset('storage/Welcome/Study.png') }}" class="img-fluid rounded-top" alt="" />
            </div>
        </div>

        <div class="row align-items-center mt-5 d-flex">

            <div class="col-6 align-items-center text-center ">
                <img src="{{ asset('storage/Thumbnails/TN_Study.png') }}" class="img-fluid rounded-top" alt="" />
            </div>
            <div class="col-6 welcome-card align-items-center d-flex">
                <div class="card text-white info-card">
                    <div class="card-body">
                        <h4 class="card-title">Internship</h4>
                        <p>
                            Join us on a journey of innovation, where our team of skilled professionals not only
                            builds
                            exceptional software and websites but also shares our knowledge and expertise with
                            aspiring
                            young minds. Our internship programs offer high-school students a hands-on learning
                            experience,
                            empowering them with the skills and guidance needed to thrive in the world of
                            programming.

                            Explore our range of services, learn more about our commitment to excellence, and
                            discover how
                            we're shaping the future of technology while fostering the growth of aspiring
                            developers.
                        </p>
                    </div>
                    <div class="text-center m-2">
                        <a href="#" class="btn btn-warning">Read more</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row  d-flex align-items-center mt-5">
            <div class="col-6 welcome-card align-items-center d-flex">
                <div class="card text-white info-card">
                    <div class="card-body">
                        <h4 class="card-title text-warning">Revolutionizing Education Through Serious Video Games</h4>
                        <p>
                            At Lionsdale Studio, we are focusing on educational innovation, leveraging the power
                            of serious video games to transform the learning landscape. Spearheaded by our visionary CEO,
                            Dr. Péter-Szabó Richárd, an expert of the field of creating educational games, we are
                            committed to pushing the boundaries of traditional learning methodologies.

                            Our serious video games go beyond entertainment; they are designed to engage and
                            educate, providing a dynamic and immersive learning experience. We integrate pedagogical
                            principles with game design, ensuring
                            that each interactive experience is not only captivating but also academically enriching.
                        </p>
                    </div>
                    <div class="text-center m-2">
                        <a href="#" class="btn btn-warning">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-6 align-items-center text-center ">
                <img src="{{ asset('storage/Welcome/seriousgames.png') }}" class="img-fluid rounded-top" alt="" />
            </div>
        </div>

        <div class="row align-items-center mt-5 pb-4 d-flex border-left">

            <div class="col-6 align-items-center text-center ">
                <img src="{{ asset('storage/Welcome/development.png') }}" class="img-fluid rounded-top" alt="" />
            </div>
            <div class="col-6 welcome-card align-items-center d-flex">
                <div class="card text-white info-card">
                    <div class="card-body">
                        <h4 class="card-title">Web and Software Development</h4>
                        <p>
                            Our skilled team of web developers excels in creating responsive, visually stunning, and
                            feature-rich websites. From sleek corporate platforms to dynamic e-commerce solutions, we
                            leverage the latest technologies to ensure a seamless and engaging user experience. Whether
                            you're launching a new site or revamping an existing one, trust us to elevate your online
                            presence.
                        </p>
                        <p>
                            On the other side we understand the critical role software plays in today's business
                            landscape. Our expert software development team is dedicated to crafting robust, scalable, and
                            customized solutions tailored to meet your specific needs. From concept to deployment, we work
                            collaboratively with you to turn your ideas into powerful, functional software that drives
                            success.
                        </p>
                    </div>
                    <div class="text-center m-2">
                        <a href="#" class="btn btn-warning">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
