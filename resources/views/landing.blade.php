<!DOCTYPE html>
<html lang="hu">
<!-- TODO: main pag -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

        <!-- Scripts -->
        @vite(['resources/sass/app.scss', 'resources/js/app.js'])


</head>

<body>

@include('assets/background_video', ['video' => 'what_to_learn.mp4'])


    <div id="content" class="d-flex h-100">
        <div id="" class="row align-self-center w-100">
            <div class="row align-self-center w-100">
                <div class="col-4 offset-3">
                    <h1 style="font-size: 46px; font-family: Source Code Pro;">Lionsdale Studio</h1>
                </div>
            </div>
            <div class="row align-self-center w-100">
                <div class="col-4 offset-3 d-flex align-items-center">
                        <span class="me-2">Say</span> <img src="{{asset('storage/Welcome/friend.png')}}" class="img-fluid allign-middle"
                            alt="friend" id="enterImg"> <span class="ms-2">and enter!</span>
                </div>
            </div>
            <div class="row align-self-center w-100 mt-3">
                <div class="col-4 offset-3">
                    <input name="enter_input" id="enter_input" type="text" class="btn btn-outline-dark"
                        style="font-family: Source Code Pro;" onchange="enter(this)">
                </div>
            </div>
            <div class="row align-self-center w-100 mt-3" id="landingError" style="visibility: hidden;">
                <div class="col-4 offset-3">
                    <figcaption class="m-1 blockquote-footer" id="landingSuccess">Oops, wrong answer. But we'll let you in.</figcaption>
                    <a href="{{route('infos.index')}}" class="btn btn-dark offset-3" id="enterButton" style="visibility: hidden;">Enter</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        function enter(input) {
            console.log(input.value);
            if (input.value.toLowerCase() == "friend") {
                document.getElementById("landingError").style.visibility = "";
                document.getElementById("enterButton").style.visibility = "";
                document.getElementById("landingSuccess").innerHTML = "Welcome, please enter!"
            }
            else {
                document.getElementById("landingError").style.visibility = "";
                document.getElementById("enterButton").style.visibility = "hidden";
                document.getElementById("landingSuccess").innerHTML = "Oops, wrong answer, Friend. Try again!"
            }
        }

        const enterInput = document.getElementById('enter_input');

        enterInput.addEventListener('keydown', function (event) {
            if (event.key === 'Enter' || event.keyCode === 13) {
                enter(enterInput);
            }
        });
    </script>
</body>

</html>