<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>
    
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    
    {{-- Fontawesome --}}
    <script src="https://kit.fontawesome.com/b253f3e6f5.js" crossorigin="anonymous"></script>
    
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    
    {{-- Trix Editor --}}
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/trix@2.0.8/dist/trix.css">
    <script type="text/javascript" src="https://unpkg.com/trix@2.0.8/dist/trix.umd.min.js"></script>
    
    {{-- SAJÁT CSS VAGY SAJÁT JS IMPORTÁLÁSA --}}
    {{-- Ha direktbe akarom belinkelni a css-t a resourcesból --}}
    {{-- @vite(['resources/css/app.css']) --}}
    
    {{-- Ha a public mappában van elhelyezve saját forrás --}}
    {{-- <link rel="stylesheet" href="{{asset("assets/css/app.css")}}"> --}}
    
    {{-- Jelenleg a resources/sass/app.scsss fájlban van importálva --}}
    
    {{-- Jquery --}}
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
    integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    
    {{-- JQUERY TABLE --}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    
    {{-- Full Calendar --}}
    <script src='https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@6.1.10/index.global.min.js'></script>
    
</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ Auth::check() ? route('home') : route('infos.index') }}">
                <img src="{{ asset('storage/Welcome/logo.png') }}" alt="Lionsdale" class="img-fluid"
                    style="max-height: 25px">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav me-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ms-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <span class="mt-1"><img class="profile-img-nav"
                                src="{{ asset('storage/ProfileImages/' . Auth::user()->profile_image) }}"
                                alt="User's Profile Image"></span>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-end bg-dark" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item nav-dropdown"
                                    href=" {{ route('user.edit', Auth::user()) }} ">Profile</a>
                                <hr class="text-white">
                                <a class="dropdown-item nav-dropdown" href="{{ route('lessons.index') }}">Lessons</a>
                                <a class="dropdown-item nav-dropdown" href="{{ route('lessons.create') }}">Create
                                    Lesson</a>
                                <a class="dropdown-item nav-dropdown" href="{{ route('lessons.show_deleted') }}">Deleted
                                    Lessons</a>
                                <hr class="text-white">
                                <a class="dropdown-item nav-dropdown" href="{{ route('workplaces.index') }}">Workplaces</a>
                                <a class="dropdown-item nav-dropdown" href="{{ route('workplaces.create') }}">Create
                                    Workplace</a>
                                <a class="dropdown-item nav-dropdown" href="{{ route('workplaces.show_deleted') }}">Deleted
                                    Workplaces</a>
                                <hr class="text-white">
                                <a class="dropdown-item nav-dropdown" href="{{ route('courses.index') }}">Courses</a>
                                <a class="dropdown-item nav-dropdown" href="{{ route('courses.create') }}">Create
                                    Course</a>
                                <a class="dropdown-item nav-dropdown" href="{{ route('courses.show_deleted') }}">Deleted
                                    Courses</a>
                                <hr class="text-white">
                                <a class="dropdown-item nav-dropdown" href="{{ route('teachers.index') }}">Teachers</a>
                                <a class="dropdown-item nav-dropdown" href="{{ route('teachers.create') }}">Add Teacher</a>
                                <hr class="text-white">
                                <a class="dropdown-item nav-dropdown" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest

                </ul>
            </div>
        </div>
    </nav>

    <div id="app">
        <main class="py-4" style="flex: 1; min-height:100%">
            @yield('content')
        </main>
    </div>

    <div class="text-white align-items-center justify-content-center mt-5 d-flex" style="background:#212529;">
        <h5 class="p-2">Copyright <i class="fa-regular fa-copyright"></i> 2024</h5>
    </div>

</body>

</html>
