@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="card bg-dark">
                <div class="card-body ">
                    <h4 class="card-title">Oh into the elf...</h4>
                    <p class="card-text text-white">There is no lesson yet, stay tuned.</p>
                </div>
            </div>
        </div>
        <div class="col-3"></div>
    </div>
    <div class="row mt-5">
        <div class="col-3"></div>
        <div class="col-6 text-center">
            <img src="{{asset('storage/Errors/sadLion.png')}}" alt="">
        </div>
        <div class="col-3"></div>
    </div>
@endsection
