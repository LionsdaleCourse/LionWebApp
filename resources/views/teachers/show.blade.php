@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col">
        <div class="card bg-dark text-white border-dark">
            <div class="card-body text-center">
                <h4 class="card-title">{{$teacher->name}}</h4>

                <img class="img-fluid mb-3" src="{{asset('storage/ProfileImages/'.$teacher->picture)}}" alt="Teacher Profile Image">
                <p>Teacher name: {{$teacher->name}}</p>
                <p>Teacher degree: {{$teacher->degree}}</p>
                <p>Teacher email: {{$teacher->email}}</p>
                <p>Teacher subjects: {{$teacher->subjects}}</p>
                
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>

@endsection