@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col"></div>
        <div class="col">
            <div class="card bg-dark text-white border-secondary">
                <div class="card-body">
                    <h4 class="card-title">Create a Workplace</h4>
                    <p class="card-text text-warning">Inputs marked with * shall be filled.</p>

                    @if ($errors->any())
                        <div class="mb-3 mt-3">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                                <strong>Holy guacamole!</strong>

                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                         
                            <strong>Holy guacamole!</strong> 
                            <p>{{Session::get('success')}}</p>
                        </div>
                    @endif

                    <form action="{{ route('workplaces.update', $resource) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" name="name" id="name" class="form-control @if($errors->has('name')) border border-danger @endif"
                                placeholder="Course Name" aria-describedby="helpId" value="{{old('name', $resource->name)}}">

                            @if ($errors->has('name'))
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                            @else
                                <small id="helpId" class="text-warning">The name of the course to be created.</small>
                            @endif

                        </div>
                        <div class="mb-3">
                            <label for="location" class="form-label">Location</label>
                            <input type="text" name="location" id="location" class="form-control"
                                placeholder="Address of your company" aria-describedby="helpId" maxlength="255" value="{{old('location', $resource->location)}}">
                            <small id="helpId" class="text-warning">Short address of the location (255 chars)</small>
                        </div>
                        <div class="mb-3">
                            <label for="registrynumber" class="form-label">Registry number</label>
                            <input type="text" name="registrynumber" id="registrynumber" class="form-control"
                                aria-describedby="helpId" value="{{old('registrynumber', $resource->registrynumber)}}">
                            <small id="helpId" class="text-warning">Registry number of the company (max res: 250*250)</small>
                        </div>
                        <div class="mb-3">
                            <label for="taxnumber" class="form-label">Taxnumber number</label>
                            <input type="text" name="taxnumber" id="taxnumber" class="form-control"
                                aria-describedby="helpId" value="{{old('taxnumber', $resource->taxnumber)}}">
                            <small id="helpId" class="text-warning">Tax number of the company (max res: 250*250)</small>
                        </div>
                        <button type="submit" class="btn form-button">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>

@endsection
