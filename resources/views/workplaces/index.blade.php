@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            @if (Session::has('success'))
                <div class="alert alert-dark alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                    <strong>Holy guacamole!</strong>
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
            {{-- Táblázat eleje --}}
            <div class="table-responsive">
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Address</th>
                            <th scope="col">Registry Number</th>
                            <th scope="col">Tax Number</th>
                            <th colspan="3">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($resources as $item)
                            <tr class="">
                                <td scope="row">{{ $item->name }}</td>
                                <td>{{ $item->location }}</td>
                                <td>{{ $item->registrynumber }}</td>
                                <td>{{ $item->taxnumber }}</td>
                                <td>
                                    <form action="{{ route('workplaces.show', $item) }}" method="get">
                                        @csrf
                                        <button type="submit" class="btn btn-info">Show</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ route('workplaces.edit', $item) }}" method="get">
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Edit</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ route('workplaces.destroy', $item) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-warning">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            {{-- Táblázat vége --}}
        </div>
        <div class="col-2"></div>
    </div>
@endsection
