@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col">
        <div class="card bg-dark text-white border-secondary">
            <div class="card-body">
                <h4 class="card-title">{{$workplace->name}}</h4>
                <p>A munkahely neve: {{$workplace->name}}</p>
                <p>A munkahely címe: {{$workplace->location}}</p>
                <p>A munkahely cégjegyzékszáma: {{$workplace->registrynumber}}</p>
                <p>A munkahely adószáma: {{$workplace->taxnumber}}</p>
                
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>

@endsection