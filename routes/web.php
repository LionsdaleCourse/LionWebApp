<?php

use App\Http\Controllers\EmailController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\TeacherController;
use App\Http\Middleware\Authenticate;
use App\Http\Middleware\VerifyUserRole;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*
|--------------------------------------------------------------------------
| Landing route and landing page
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return view('landing');
})->name('landing');

/*
|--------------------------------------------------------------------------
| Authentication routes and landing page
|--------------------------------------------------------------------------
*/

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(Authenticate::class)->group(function () {
    /*
    |--------------------------------------------------------------------------
    | Courses routes
    |--------------------------------------------------------------------------
    */

    Route::resource('/courses', App\Http\Controllers\CourseController::class);
    Route::get('/course/show_deleted', [App\Http\Controllers\CourseController::class, 'show_deleted'])->name('courses.show_deleted');
    Route::put('/courses/restore/{course}', [App\Http\Controllers\CourseController::class, 'restore'])->name('courses.restore')->withTrashed();

    /*
    |--------------------------------------------------------------------------
    | Workplace routes
    |--------------------------------------------------------------------------
    */

    Route::get('/workplaces/show_deleted', [App\Http\Controllers\WorkplaceController::class, 'show_deleted'])->name('workplaces.show_deleted');
    Route::put('/workplaces/restore/{workplace}', [App\Http\Controllers\WorkplaceController::class, 'restore'])->name('workplaces.restore')->withTrashed();
    Route::resource('/workplaces', App\Http\Controllers\WorkplaceController::class);

    /*
    |--------------------------------------------------------------------------
    | Lessons routes
    |--------------------------------------------------------------------------
    */
    Route::get('/lessons/show_deleted', [App\Http\Controllers\LessonController::class, 'show_deleted'])->name('lessons.show_deleted')->middleware(VerifyUserRole::class);
    Route::get('/lessons/{course}/show_lessons', [App\Http\Controllers\LessonController::class, 'show_lessons'])->name('lessons.show_lessons');
    Route::resource('/lessons', App\Http\Controllers\LessonController::class);


    /*
    |--------------------------------------------------------------------------
    | Authenticated User Routes
    |--------------------------------------------------------------------------
    */

    Route::get('user/{user}/updateprofile', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
    Route::put('user/{user}', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');

    /*
    |--------------------------------------------------------------------------
    | Course Review Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('/review', ReviewController::class);

});





/*
|--------------------------------------------------------------------------
| Info User Routes
|--------------------------------------------------------------------------
*/

Route::resource('/infos', InfoController::class);

//Training facility rental, Internship, Serious games, Web and Software development - aloldalt, controller nélkül
// Cím, egy képnek hely, leírás

//Custom 404 és 403 ->error oldal az oldal stílusában (Ha a workplacesnél a felhasználó role_id != 3 akkor dobjon ide)

// Kapcsolatok aloldal, üzenetküldésre, szerkesztőfelülettel (Cím, 
//alatta bal oldalt szöveg (lionsdale címe, telefon, email elérhetőség, adószám, név, ügyvezető, 
//JOBB oldalt egy kép, 
//ALATTA szövegszerkesztő (ennekcsak egy sort csináljatok és írjátok bele h szövegszerkesztő)))


/*
|--------------------------------------------------------------------------
| Teacher Routes
|--------------------------------------------------------------------------
*/

Route::resource('/teachers', TeacherController::class);

/*
|--------------------------------------------------------------------------
| Email Routes
|--------------------------------------------------------------------------
*/

Route::get('/sendConfirmation', [EmailController::class, 'sendWelcomeEmail']);